class Config {
  static const String appName = "Veegil Bank App";
  static const String apiURL = "10.0.2.2:8000";
  static const loginAPI = "/auth/login";
  static const signupAPI = "/auth/signup";
  static const transferAPI = "/accounts/transfer";
  static const withdrawAPI = "/accounts/withdraw";
  static const transactionAPI = "/transactions";
  static const singleTransactionAPI = "/accounts/transactions";

  static const userProfileAPI = "/auth/user-Profile";
}
