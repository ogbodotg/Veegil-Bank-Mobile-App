# veebank

OGBODO THANKGOD
Veegil Bank Mobile App built with Flutter (frontend) and NodeJS (backend).
Create Account, Login, Transfer, Withdraw, and get Transaction History.

So, i deployed the API to Heroku and then compiled the an apk file available here on Google Drive https://drive.google.com/file/d/13pGqYwVBU8rW7F0TfoExXiLvfDLDkYuW/view?usp=sharing 

To clone and test the app, please see instruction below.

To test this app, first clone the NodeJS Api https://gitlab.com/ogbodotg/Veegil-Bank-API.git (see instruction on how to clone and run API server => https://gitlab.com/ogbodotg/Veegil-Bank-API/-/blob/master/README.md).

## Getting Started
After cloning the Veegil Bank API, also clone this repo, run flutter pub get to install all dependencies and then run app on virtual emulator.

I think i did a great and amazing job, but I still think i can make more improvements to the project too. I was only trying to work and complete the major features within the timeframe given. And i had just few days because of the bad power issue. I however welcome your criticism, suggestion etc, as it would help me do better.
